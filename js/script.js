$(document).ready(function() {
    quoteGenerator();

    $('#quoteGenerator').on('click', function() {
      quoteGenerator();
    });

    $('#quoteTwitterShare').on('click', function() {
      var tweetThis = $('#quoteArea').text() + '\n' + $('#quoteAuthor').text();
      $(this).attr("href", "https://twitter.com/intent/tweet?text=" + tweetThis);
    });
});

function quoteGenerator() {
  var url = 'https://api.forismatic.com/api/1.0/?method=getQuote&format=jsonp&lang=en&jsonp=parseQuote'; 

  $.ajax({
    dataType: 'jsonp',
    jsonpCallback: "parseQuote",
    url: url,
    success: function(response) {
      // console.log(response);

      var myQuote = response;
      console.log(myQuote);
      var quoteText = myQuote.quoteText;
      var quoteAuthor = '     - ' + myQuote.quoteAuthor;

      console.log('Text: ' + quoteText);
      console.log('Author: ' + quoteAuthor);

      $('#quoteArea').text(quoteText);
      $('#quoteAuthor').text(quoteAuthor);

    }
  });
}

